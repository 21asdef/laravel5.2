@extends('main')
@section('title','Home')
    
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h1 class="display-4">Welcome to my Blog!</h1>
                <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information ...</p>
                <p><a class="btn btn-primary btn-lg" href="#" role="button">Popular Post</a></p>
            </div>
        </div>
    </div> <!-- end of .row jumbotron -->

    <div class="row">
        <div class="col-md-8">
            @foreach ($posts as $post)
                <div class="post">
                <h3>{{ $post->title }}</h3>
                    <p>{{ $post->body }}</p>
                <a href="posts/{{ $post->id }}" class="btn btn-primary">Read More</a>
                </div>
                <hr>
            @endforeach
        </div>
        <div class="col-md-4 col-md-offsset-1">
            <h2>Sidebar</h2>
        </div>
    </div>
@endsection
