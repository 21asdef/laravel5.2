@extends('main')

@section('title','Create New Post')

@section('stylesheets')
    {!! Html::style('css/parsley.css') !!}
@endsection

@section('content')
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <h1>Create New Post</h1>
        <hr>
        {!! Form::open(['route' => 'posts.store','data-parsley-validate' => '']) !!}
          {{ Form::label('title','Title: ') }}
          {{ Form::text('title',null,['class' => 'form-control','required' => '', 'maxlength' => '255']) }}

          {{ Form::label('body','Post Body: ') }}
          {{ Form::textarea('body',null,['class' => 'form-control', 'required' => '']) }}
          <br>
          {{ Form::submit('Create Post',['class' => 'btn btn-success btn-lg btn-block']) }}
        {!! Form::close() !!}
      </div>
      <div class="col-md-2"></div>
    </div>
@endsection

@section('scripts')
    {!! Html::script('js/parsley.min.js') !!}
@endsection