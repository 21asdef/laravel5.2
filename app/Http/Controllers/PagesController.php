<?php

namespace App\Http\Controllers;

use App\Post;

 class PagesController extends Controller {

  public function getIndex(){
    $posts = new Post;

    $posts = Post::all();
    return view('pages/home')->withPosts($posts);
  }

  public function getAbout(){
    $first = "Jeremie Vinze";
    $last = "Cuison";

    $fullname = $first." ".$last;
    $email = "jeremievinzec@gmail.com";

    $data = [];
    $data['email'] = $email;
    $data['fullname'] = $fullname;

    return view('pages/about')->withData($data);
  }

  public function getContact(){
    return view('pages/contact');
  }
  
 }